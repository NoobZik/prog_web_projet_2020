<?php

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index.php                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 00:33:36 by noobzik           #+#    #+#             */
/*   Updated: 2020/03/05 00:33:36 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


//include 'includes/autoloader.inc.php';

require_once (__DIR__.'/core/init.php');


?>

<!DOCTYPE HTML>
<html lang="fr" class="h-100">
<head>
    <title>PROJECT OXYGEN : FAC</title>
    <meta charset="utf-8" />
    <meta name="description" content="Le jeu des capitales teste vos compétences en géographie pour retrouver des pays et leurs capitales sur une carte" />
    <meta name="keywords" content="jeu capitales géographie geo pays carte monde europe afrique etats unis oceanie australie points score">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script src="js/jQuery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/SweetAlert2.js"></script>
    <script src="js/swal_customs.js"></script>
    <script src="js/bootstrap.bundle.js"></script>


    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/SweetAlert2.css" />
    <link rel="stylesheet" href="css/flatty.css" />
    <link rel="stylesheet" href="css/style.css" />

    <!--script src="js/jquery.js"></script-->
</head>
<body class="d-flex flex-column h-100">
<?php include "includes/menu.php";?>
<section class="jumbotron">
    <div class="container-fluid">
        <?php
        include_once (__DIR__."/includes/loginProcess.inc.php");
        ?>
        <div class="container-fluid">
            <h1>Best scores wall</h1>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Username</th>
                        <th scope="col">Score</th>
                        <th scope="col">Date</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                // Recupération des données de score depuis la base sql et mise en forme de table
                $result = DataBase::getInstance();
                $sql = "SELECT username, score, date FROM `score` natural join users where userID = id order by score desc;";
                $scoreArray = $result->query($sql, array())->results();
               // var_dump($scoreArray);
                if (!count($scoreArray)) {
                    echo '<div class="alert alert-info" role="alert">Oh there\'s still no data to be shown</div>';
                }
                else {
                    for ($i = 0; $i < count($scoreArray); $i++) {
                        $username = $scoreArray[$i]->username;
                        $score = $scoreArray[$i]->score;
                        $date = $scoreArray[$i]->date;
                        ?>
                        <tr>
                            <th scope="row"><?php echo $i + 1; ?></th>
                            <td><?php echo $username; ?></td>
                            <td><?php echo $score; ?></td>
                            <td><?php echo $date; ?></td>
                        </tr>
                        <?php
                    }
                }
                    // Afficher rien
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php include(__DIR__."/includes/footer.inc.php"); ?>

</body>


</html>
