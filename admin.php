<?php

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index.php                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 00:33:36 by noobzik           #+#    #+#             */
/*   Updated: 2020/03/05 00:33:36 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


//include 'includes/autoloader.inc.php';

require_once (__DIR__.'/core/init.php');


?>

<!DOCTYPE HTML>
<html lang="fr" class="h-100">
<head>
    <title>PROJECT OXYGEN : FAC</title>
    <meta charset="utf-8" />
    <meta name="description" content="Le jeu des capitales teste vos compétences en géographie pour retrouver des pays et leurs capitales sur une carte" />
    <meta name="keywords" content="jeu capitales géographie geo pays carte monde europe afrique etats unis oceanie australie points score">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <script src="js/jQuery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/SweetAlert2.js"></script>
    <script src="js/swal_customs.js"></script>
    <script src="js/bootstrap.bundle.js"></script>


    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/SweetAlert2.css" />
    <link rel="stylesheet" href="css/flatty.css" />
    <link rel="stylesheet" href="css/style.css" />

    <!--script src="js/jquery.js"></script-->
</head>
<body class="d-flex flex-column h-100">
<?php include "includes/menu.php";?>
<div class="container">
    <h1>Panel d'administation des comptes Utilisateurs</h1>
</div>
<?php include(__DIR__."/includes/footer.inc.php"); ?>

</body>


</html>
