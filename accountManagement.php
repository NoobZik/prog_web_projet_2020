<?php

include_once(__DIR__ . '/core/init.php');

?>
<!DOCTYPE HTML>
<html lang="fr" class="h-100">
<head>
    <title>PROJECT OXYGEN : Account Managemnt</title>
    <meta charset="utf-8"/>
    <meta name="description"
          content="Le jeu des capitales teste vos compétences en géographie pour retrouver des pays et leurs capitales sur une carte"/>
    <meta name="keywords"
          content="jeu capitales géographie geo pays carte monde europe afrique etats unis oceanie australie points score">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script src="js/jQuery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="js/SweetAlert2.js"></script>
    <script src="js/swal_customs.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Installation Bootstrap v4 -->
    <link rel="stylesheet" href="css/bootstrap.css"/>
    <link rel="stylesheet" href="css/SweetAlert2.css"/>
    <link rel="stylesheet" href="css/flatty.css"/>

    <link rel="stylesheet" href="css/style.css"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"/>
</head>

<body class="d-flex flex-column h-100">
<?php
include(__DIR__ . "/includes/menu.php");

if (!isset($_SESSION["username"])) {
    ?>
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Oh no!</h4>
        <p>Aww no, you somehow managed to land on this page without connecting !.</p>
        <hr>
        <p class="mb-0">In order to edit your account details, you need to log in first (On the top left corner).</p>
    </div>
    <?php
} else {
    $newDate = date("Y-m-d", strtotime($_SESSION["signUpDate"]));
    ?>
    <section class="jumbotron">
        <div class="container-lg">

            <h1>Account details</h1>
            <p class="alert alert-info">Here, you can review your account information</p>

            <div class="form-group">
                <form>
                    <fieldset disabled>
                        <label for="username">User ID</label>
                        <input id="userID" class="form-control" type="text"
                               value="<?php echo $_SESSION["userID"] ?>" readonly/>
                        <label for="username">Username</label>
                        <input id="username" class="form-control" type="text"
                               value="<?php echo $_SESSION["username"] ?>" readonly/>
                        <label for="signUpDate">Sign up date</label>
                        <input class="form-control" type="date" value="<?php echo $newDate ?>" readonly/>
                        <label for="email">Email Address</label>
                        <input type="email" class="form-control" id="email" value="<?php echo $_SESSION["email"] ?>" readonly/>
                    </fieldset>


                </form>
            </div>
            <button type="button" class="btn btn-info" onclick="editPassword()">Edit password</button>
        </div>

    </section>

    <?php
}
include(__DIR__ . "/includes/footer.inc.php"); ?>
</body>
</html>

<script>
    function editPassword() {
        Swal.mixin({
            progressSteps: ['1', '2']
        }).queue([
            {
                title: 'Editing your password process',
                html: 'Fill out this form to edit your password !' +
                    '<div class="container">' +
                    '<input type="password" name="oldPassword" id="oldPassword" class="swal2-input" placeholder="Enter your current password" value="" required>' +
                    '<div class="custom-control custom-checkbox">' +
                    '<input type="checkbox" class="custom-control-input" id="customCheck1" >' +
                    '<label class="custom-control-label" for="customCheck1"  onclick="showPassword(1)">Show/Hide Password</label>' +
                    '</div>',
                preConfirm: () => {
                    var oldPassword = Swal.getPopup().querySelector('#oldPassword').value;
                    if (oldPassword === "") {
                        Swal.showValidationMessage(`A password can't be empty !`)
                    } else {
                        $.ajax({
                            url: 'includes/login.inc.php',
                            async: false,
                            type: "POST",
                            data: {"oldPassword": oldPassword},
                            success: function (data) {
                                if (data.toString() === "false")
                                    Swal.showValidationMessage(`Verification Failure`)
                            }
                        });
                    }
                }
            },
            {
                title: 'Enter your new password',
                html:
                    '<div class="container">' +
                    '<input type="password" name="password1" id="password1" class="swal2-input" placeholder="Enter your password" required>' +
                    '<div class="custom-control custom-checkbox">' +
                    '<input type="checkbox" class="custom-control-input" id="customCheck1" >' +
                    '<label class="custom-control-label" for="customCheck1"  onclick="showPassword(1)">Show/Hide Password</label>' +
                    '</div>' +
                    '<input type="password" name="password2" id="password2" class="swal2-input" placeholder="Re-enter your password" required>' +
                    '<div class="custom-control custom-checkbox">' +
                    '<input type="checkbox" class="custom-control-input" id="customCheck2" >' +
                    '<label class="custom-control-label" for="customCheck2"  onclick="showPassword(2)">Show/Hide Password</label>' +
                    '</div>' +
                    '</div>',
                confirmButtonText: 'Apply',
                preConfirm: () => {
                    let password1 = Swal.getPopup().querySelector('#password1').value;
                    let password2 = Swal.getPopup().querySelector('#password2').value;
                    if (password1 === '' || password2 === '') {
                        Swal.showValidationMessage(`Password required`)
                    }
                    if (password1.localeCompare(password2) !== 0) {
                        Swal.showValidationMessage(`Password does not match`)
                    } else {
                        $.ajax({
                            url: 'includes/login.inc.php',
                            async: false,
                            type: "POST",
                            data: {"newPassword": password2},
                            success: function (data) {
                                console.log(data)
                                if (data === "true") {
                                    Swal.fire({
                                        title: 'Success !',
                                        text: 'Your password is changed successfully',
                                        icon: 'success'
                                    })
                                } else {
                                    Swal.fire({
                                        title: 'Failure !',
                                        text: 'An internal error has occurred',
                                        icon: 'error'
                                    })
                                }
                            }
                        });
                    }
                }
            }])
    }
</script>