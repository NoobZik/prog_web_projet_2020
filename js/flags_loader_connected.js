/**
 * Main function called after the loading.
 * Configure the quiz according to the user preferences with a step by step interaction
 *
 * 1st Step is choosing the quiz plate between 8 continents
 *
 * 2nd Step is choosing the amount of question for the session (Should be only available
 * for registered users). Ensure that the input value is a number before proceeding to the
 * next step.
 *
 * 3rd Step Calls the function getQuestion with the user configuration.
 *
 */
function loader() {
    counter = 0;
    score = 0;
    questionNumber = 0;
    Swal.mixin({
        allowOutsideClick: false,
        progressSteps: ['1', '2', '3']
    }).queue([
        {
            title: 'Choose a continent',
            input: 'select',
            inputOptions: {
                WO: 'World',
                AF: 'Africa',
                AN: 'Antarctica',
                AS: 'Asia',
                EU: 'Europe',
                NA: 'North America',
                SA: 'South America',
                OC: 'Oceania'
            },
            inputValue: "AF"
        },
        {
            title: 'Choose an amount of question',
            input: 'number',
            inputPlaceholder: 'Type a number above 0',
            preConfirm: (inputValue) => {
                if (inputValue <= 0)
                    Swal.showValidationMessage("Must be above 0");
            }
        }
    ]).then((result => {
        if (result.value) {
            const answers = result.value
            Swal.fire({
                title: 'All done!',
                icon: 'success',
                text: 'Alright, everything is now set up !'
            })
            question = getQuestions(answers[0], answers[1]);
        }
    }))
}

loader();