var map = L.map('map').setView([25, 0], 3);
var counter = 0;
var score = 0;
var questionNumber = 0;
// load a tile layer with a defaut starting point

L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.{ext}', {
    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    subdomains: 'abcd',
    minZoom: 1,
    maxZoom: 16,
    ext: 'jpg',
}).addTo(map);


/**
 * getQuestion
 * Ask the php server to generate a sets of question for the Quiz.
 * Ensure that the received json data is correctly parsed before it can
 * be processed by the quiz (Throw an error otherwise)
 * @param continent
 * @param number
 * @returns {[]}
 */
function getQuestions (continent, number) {
    let question = [];
    $.ajax({
        url:'includes/countries.inc.php',
        async :false,
        type : 'POST',
        data: {
            'continent': continent,
            'number': number
        },
        success: function (data) {
            try {
                question = JSON.parse(data);
            }
            catch (e) {
                Swal.fire({
                    title: "An internal error as occurred while parsing the PHP JSON !",
                    text: "Try to reload the page, it might work.."
                });
                throw new Error("Json not loaded properly");
            }
            $("#flag").attr("class", 'flag-icon flag-icon-' + question[0].code.toLowerCase());
        },
        error: function () {
            Swal.fire({
                title: "An internal error as occurred !",
                text: "Try to reload the page, it might work.."
            });
        }
    });

    return question;
}

/**
 * Load the dataset polygons into leaflet
 * Set the style to each polygons for mouseover/mouseout event
 * Add a verification function to each layer in order to interact with the quiz.
 */
$.getJSON("js/countries-min.json", function(data) {
    // Apply a popup event to each "Features" of the dataset
   L.geoJSON(data, {
        async:false,
        onEachFeature: function (feature, layer) {
            layer.setStyle({fillColor: '#000000', stroke: 'false'});
            layer.on('click', verifyAnswer);
            layer.on('mouseover', function () {
                this.setStyle({
                    'fillColor': '#e67e22'
                });
            });
            layer.on('mouseout', function () {
                this.setStyle({
                    'fillColor': '#000000',
                });
            });
        }
    }).addTo(map);
});

var question;



/**
 * Check if the clicked layer matches to the requested flag typed in ALPHA-2 standard.
 *
 * *  catch an error if the parser does not recognize a ALPHA-2 standard and report it as
 * an error to the console log
 *
 * *  If the answer is correct, show a Sweet-alert success notification with a timer of 2s.
 *    with a country name
 * *  Else show a error notification with a timer of 1.5s.
 * *  Both does not show a confirmation button.
 * @param e
 */
function verifyAnswer(e) {
    var layer = e.target;
    var flag = layer.feature.properties.cca2.toLowerCase();
    var name = question[questionNumber].name;
    try {
        var requested = parserFlag($("#flag").attr("class"));
    }
    catch (e) {
        console.error(e);
    }

    tryCounter();

    if (requested.localeCompare(flag) === 0) {
        Swal.fire({
            title: "This is correct !",
            icon: "success",
            html: '<p>The country name was '+ name +'<p/>' +
                '<a href="https://en.wikipedia.org/wiki/'+name+'" target="_blank">Click here to know more about this country</a>',
            showConfirmButton: true,
        }).then(scoreCounter).then(nextQuestion)
    }
    else {
        if (counter < 3)
        Swal.fire ({
            icon: "error",
            html: '<p class="timer">Number of try : '+ counter +'/3</p>',
            showConfirmButton: true,
            timer: 1500
        })
        else {
            Swal.fire ({
                icon: "error",
                title: "You failed to pinpoint :" + question[questionNumber].name +"!",
                html: '<p>The location was : '+ counter +'/3</p>'+
                    '<a href="https://en.wikipedia.org/wiki/'+name+'" target="_blank">Click here to know more about this country</a>',
                showConfirmButton: true,
            }).then(nextQuestion);

        }
    }

}

/**
 * parse an span attribute to only keep the ALPHA-2 code
 * This function is used to get the current displayed on screen flag
 * @param flag
 * @returns {*}
 */
function parserFlag(flag) {
    if (flag.length === 22)
        return res = flag[20] + flag[21];
    else throw 'Not a ALPHA-2 format' + flag + ' ' +flag[20] + flag[21];
}

/**
 * Iterate to the next question and sets the counter to 0
 * Also update the displayed on screen flag by changing the class attribute
 * End the quiz if there's no more question to be asked
 * When the quiz is ended, asks the user if they want to registry their score and if they
 * want to take the quiz again.
 */
function nextQuestion() {
    questionNumber++;
    counter = 0;
    if (questionNumber >= question.length) {
        Swal.fire({
            title: 'Quiz Finished',
            text: 'You finished with a score of ' + score + '\n Refresh to play again',
            html:
                '<p>You finished with a score of ' + score + '</p>' +
                    '<button class="btn btn-info" onclick="sendScore(score)">' + 'Save score' + '</button>&nbsp&nbsp' +
                    '<button class="btn btn-info" onclick="loader()">' + 'Play again' + '</button>',
            showConfirmButton: false

        })
    }
    else {
        var questionActual = question[questionNumber].code.toLowerCase();
        $("#flag").attr("class", 'flag-icon flag-icon-' + questionActual);
    }
}

/**
 * Register the number of attempts
 * @returns {number}
 */
function tryCounter() {
    counter++;
}


/**
 * Count the score
 */
function scoreCounter () {
    if (counter === 1) {
        score += 3;
    }
    else if (counter === 2) {
        score += 1;
    }
   // nextQuestion();

}

/**
 * Send the final score to the server
 * @param score
 */
function sendScore(score) {
    $.ajax({
        url:'includes/Score.inc.php',
        async :false,
        type: "POST",
        data : {
            "score": score
        },
        success: function (data) {
            Swal.fire({
                title: "Hooray",
                icon: 'success',
                text: data
            })
        },
        error: function () {
            Swal.fire({
                title: "An internal error as occurred !",
                text: "Try to reload the page, it might work.."
            });
        }
    });
}


