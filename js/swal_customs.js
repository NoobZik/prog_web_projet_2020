function showPassword(id) {
    let x = document.getElementById("password"+id);
    // document.write("password"+id);
    if (x.type === "password") {
        x.type = "text";
    }
    else {
        x.type = "password";
    }
}

function login() {
    Swal.fire({
        title: 'Welcome !',
        html:
            'Please log in' +
            '<div class="container" >'+
                '<form class="form-group" name="formLogin" method="post" action="includes/login.inc.php">'+
                    '<input name="username" type="text" id="username" class="swal2-input" value="" placeholder="Enter your username">'+
                    '<input name="password" type="password" id="password0" class="swal2-input" placeholder="Enter your password">'+
                    '<div class="custom-control custom-checkbox">'+
                        '<input type="checkbox" class="custom-control-input" id="customCheck1" >'+
                        '<label class="custom-control-label" for="customCheck1"  onclick="showPassword(0)">Show/Hide Password</label>'+
                    '</div>'+
                '</form>'+
            '</div>',
        confirmButtonText: 'Login',
        preConfirm: () => {
            let username = Swal.getPopup().querySelector('#username').value;
            let password = Swal.getPopup().querySelector('#password0').value;
            if (username === '' || password === '') {
                Swal.showValidationMessage(`Username/Password empty`)
            }
            return {username: username, password: password}
        }
    }).then((result) => {
        if (result.value) {
            document.formLogin.action = "includes/login.inc.php";
            document.formLogin.method = "post";
            document.formLogin.submit();
        }
    })
}

/**
 * [signup description]
 *
 * @return  [type]  [return description]
 */
function signUp() {
    Swal.fire({
        title: 'Welcome !',
        html:
            'Fill out this form to get started'+
            '<div class="container">'+
                '<form class="form-group" name="signUpForm" method="post" action="includes/login.inc.php">'+
                    '<input type="text" name="username_reg" id="username_reg" class="swal2-input" placeholder="Enter your username"  value="" required>' +
                    '<input type="password" name="password1" id="password1" class="swal2-input" placeholder="Enter your password" required>' +
                    '<div class="custom-control custom-checkbox">'+
                        '<input type="checkbox" class="custom-control-input" id="customCheck1" >'+
                        '<label class="custom-control-label" for="customCheck1"  onclick="showPassword(1)">Show/Hide Password</label>'+
                    '</div>'+
                    '<input type="password" name="password2" id="password2" class="swal2-input" placeholder="Re-enter your password" required>' +
                    '<div class="custom-control custom-checkbox">'+
                        '<input type="checkbox" class="custom-control-input" id="customCheck2" >'+
                        '<label class="custom-control-label" for="customCheck2"  onclick="showPassword(2)">Show/Hide Password</label>'+
                    '</div>'+
                    '<input type="email" name="mail" id="mail" class="swal2-input" placeholder="Enter your email" required>'+
                '</form>'+
            '</div>',
        confirmButtonText: 'Sign Up',
        preConfirm: () => {
            let username = Swal.getPopup().querySelector('#username_reg').value;
            let password1 = Swal.getPopup().querySelector('#password1').value;
            let password2 = Swal.getPopup().querySelector('#password2').value;
            let mail = Swal.getPopup().querySelector('#mail').value;
            if (username === '' || password1 === '' || password2 === ''|| mail === '') {
                Swal.showValidationMessage(`Username/Password/mail empty`)
            }
            if (password1.localeCompare(password2) !== 0) {
                Swal.showValidationMessage(`Password does not match`)
            }
            return {username: username, password: password1, mail: mail}
        }
    }).then((result) => {
        if (result.value) {
            document.signUpForm.action = "includes/signup.inc.php";
            document.signUpForm.method = "post";
            document.signUpForm.submit();
        }
    })
}


$(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    var input = $($(this).attr("toggle"));
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    }
    else {
        input.attr("type", "password");
    }
});

function logOut() {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'You were successfully logged out',
        showConfirmButton: false,
        timer: 1500
    })
}

function loggedIn() {
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'You were successfully logged in',
        showConfirmButton: false,
        timer: 1500
    })
}

function loginError() {
    Swal.fire(
        'Error while trying to log in!',
        'Please check if your credentials are correct and try again!',
        'error'
    )
}

function registrationSuccess() {
    Swal.fire(
        'Registration success!',
        'You can now log in!',
        'success'
    )
}

function registrationFailure() {
    Swal.fire(
        'Registration failure!',
        'Maybe someone has already used that username or email!',
        'error'
    )
}