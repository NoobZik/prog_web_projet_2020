<?php

include (__DIR__.'/core/init.php');

?>
<!DOCTYPE HTML>
<html lang="fr" class="h-100">
<head>
    <title>PROJECT OXYGEN : FAC</title>
    <meta charset="utf-8" />
    <meta name="description" content="Le jeu des capitales teste vos compétences en géographie pour retrouver des pays et leurs capitales sur une carte" />
    <meta name="keywords" content="jeu capitales géographie geo pays carte monde europe afrique etats unis oceanie australie points score">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <script src="js/jQuery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/SweetAlert2.js"></script>
    <script src="js/swal_customs.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
          integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
            integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
            crossorigin=""></script>
    <!-- Installation Bootstrap v4 -->
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/SweetAlert2.css" />
    <link rel="stylesheet" href="css/flatty.css" />
    <link rel="stylesheet" href="css/flag-icon.css" />

    <link rel="stylesheet" href="css/style.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/leaflet.css" />

</head>
<body class="d-flex flex-column h-100">
<!-- Header -->
<?php include(__DIR__."/includes/menu.php"); ?>

<section class="jumbotron" >
    <div>
        <h1 class="text-center">Find the flag location !<span id="flag" class=""></span></h1>
    </div>
    <div id="map" style="width: 100%; height: 500px;"></div>
</section>

<section>
    <div class="container-fluid">
        <button class="btn btn-info" onclick="openHelpCapital();">Need help to get started ?</button>
    </div>
</section>



<!-- Footer -->
<?php include(__DIR__."/includes/footer.inc.php"); ?>

<!-- Installation des scripts JS -->
<script>
    function openHelpCapital() {
        Swal.mixin({
            confirmButtonText: 'Next &rarr;',
            showCancelButton: false,
            progressSteps: ['1', '2', '3', '4']
        }).queue([
            {
                title: 'Step 1',
                text: 'A series of question about the requested flag location will be given'
            },
            {
                title: 'Step 2',
                text: 'To answer, pin on the map the location of the flag accurately'
            },
            {
                title: 'Step 3',
                text: 'You score 3 point at one shot, 1 point if not'
            },
            {
                title: 'Step 4',
                text: 'You only have 2 attempts !'
            }]).then((result) => {
            if (result.value) {
                Swal.fire({
                    title: 'Hooray!',
                    text: 'Alright, you know the basics now !',
                    confirmButtonText: 'Lovely!'
                })
            }
        })
    }
</script>
<script src="js/flag_finder.js"></script>
<?php
if (isset($_SESSION["username"])) {
    echo '<script src="js/flags_loader_connected.js"></script>';
}
else {
    echo '<script src="js/flags_loader_guest.js"></script>';
}
?>
</body>


</html>
