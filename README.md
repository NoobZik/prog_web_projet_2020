
# Rapport de prog_web_projet_2020 #

| Nom / Prénom   | Numéro Etudiant |
| :------------- | :-------------- |
| SHEIKH Rakib   | 11502605        |
| RAHBI Sohayla  | 11604156        |

[Lien vers le répertoire](https://bitbucket.org/NoobZik/prog_web_projet_2020/commits/)

## Cahier des charges ##

Le site web devra être rédigé avec les technologies et fonctionnalités suivantes, d'une part le front-end :

[x]   HTML5

[x]   La mise en page doit se faire impérativement avec Twitter Bootstrap

[x]   Leaflet pour la cartographie

[x]   Les données utiles doivent être stockés dans un JSON et chargés avec des appels Ajax (jQuery).

d'autre part le back-end :

[x]   Les requêtes SQL doivent être écrite en PHP PDO.

[x]   Une gestion d'utilisateur avec sauvegarde d'une date d'inscription + score.

[x]   Les mots de passe devront être stocké et hashés.

[x]   Le mode invité aura maximum 5 questions.

[ ]   Un panel d'administration pour gérer les utilisateurs.

## Réponse au cahier des charges ##

### Repartition des tâches ###

Rakib SHEIKH s'est occupé principalement du backend avec les points suivants :
*   Table de base de données SQL.
*   Ecriture d'un javascript personnalisé pour la librairie SweetAlert2 gérant les connexions.
*   Mise en place d'une structure de fichier en orienté objet principalement.
*   Ecriture de la classe Database, Utilisateur, Config, Countries, Input.
*   Implémentation d'un fichier php se chargant de charger les classe de manière autonome à la demande.
*   Assurer la compatibilité avec tout type de navigateur et hardware.

RAHBI Sohayla s'est occupé principalement du Front-end avec les points suivants :
*   Design du squelette du site en générale. (CSS Bootstrap)
*   Ecriture des fonctions vitales du site (Menu, footer, body)
*   Recherche et exploitation des bases de données JSON exploitable pour leaflet.
*   La mise en communication du Front-end et le back-end par l'intermédiaire des fichiers includes du php.
*   La gestion d'utilisateur (Inscription/Connexion par les includes php)
*   La mise en place d'une fonctionnalité affichant les meilleurs score des joueurs accessible en public.

Nous avons conjointement travaillée sur la partie Javascript du quiz.


### Présentation du site en question ###

*   Le but est de créer un site de divertissement à but culturel afin de consolider ses connaissances géographiques de la planète terre.
*   Le quiz devra posseder 2 version tel que la version avec un utilisateur qui est enrengistrée sur le site dont le questionnaire est généré aléatoirement sur le contenu et sur la la quantitée à la demande. Et la version publique (c'est-à-dire utilisateur non enrengistrée sur le site) avec un questionnaire limité et fixée
*   Ce site de divertissement à but culturel est représenté sous forme d'un ensemble de quiz se portant sur différents thèmes centré sur la localisation d'un pays, région, capital et ville.
*   Le public visé est principalement les jeunes de 6-11 ans. Un autre public visés sont ceux qui travaillent pour le monde de l'aéronautique, opérations et transport logistiques.
*   A chaque fin de question, le joueur aura la possibilité de se cultiver avec la proposition de voir une page Wikipédia du pays en question.

#### Règles ####

Un plateau de la carte du monde (Terre) est proposé à l'utilisation souhaitant jouer. Le jeu prend la forme d'un quiz avec une selection d'un pays à l'aide d'un pointeur de type souris ou ses doigts pour les écrans tactiles.
Le joueur inscrit au site web devra tout d'abord configurer le quiz. La configuration est assistée et prends simplement deux étapes :
*   Choix d'un continent
*   Choix d'un nombre de questions supérieur à 0.

Le quiz prends la forme d'une question avec simplement un drapeaux affiché à l'écran. Le but du quiz sera donc de trouver la localisation de ce drapeaux sur la carte mise à disposition

L'utilisateur se verra poser x question sur le continent de son choix. Il aura droit à 3 essaies afin de trouver le pays demandé

En revanche, un joueur qui n'est pas inscrit au site web se verra imposé une liste de 5 questions.

#### Barèmes ####

*   Une réponse correcte dès le premier essaie vaut 3 points.
*   Une réponse correcte après le premier essaie vaut 1 point.
*   Une réponse non correcte après 3 essaie vaut 0 point.
*   Aucun point n'est retiré au cours du quiz.
  
## Schéma de la base de donnée ##

La conception d'une base de donnée SQL est incontournable dans le cadre de ce projet. Cependant, la base de donnée doit être le plus simple possible, et doit s'efforcer les règles de conception d'une base de données vu dans le cours de base de données au premier semestre.

De ce fait, la base de donnée qui à été proposé pour ce projet est en forme normale 3.

![Schema SQL](static/img/SQL.jpg)

## Structuration des codes ##

### Généralités d'une structuration d'un fichier de code ###

*   La rédaction des commentaires se limite au strict minimum compréhension par tout type de personnes qui inspecterai le code. En effet, on pense que trop de commentaire serait nocif à la lisibilité du fichier en question. Il en est décidé ainsi que les seules commentaires qui se trouverai servirait seulement à expliquer l'utilisation d'une fonction avec ses différents paramètres d'entrées et de sorties (Comme une sorte d'API).

*   Le projet à été codé avec une méthode dite "agile". Nous avons constament eu besoin de refactorer le code en fonction du besoins rencontré lors du codage. 

### Back End ###

*   Il a été décidé d'utiliser la méthode de l'orienté objet dans le but de maximiser la réutilisation des bouts de codes et de garder la pertinence des noms de fichiers. 

*   De plus pour bien représenter l'absorbence de l'orienté objet, chaque fichier sont dans un dossier contient impérativement le nom de son dossier afin de différencier son type de classe (soit Include ou Class). Les fichiers qui ne comportent pas ces mentions sont déstiné à l'affichage de l'interface utilisateur.

Exemples : 
-    Une classe Utilisateur implémenté en php aura le nommage suivant : `/Classe/Utilisateur.class.php`
-    Un include Utilisateur implémenté en php aura le nommage suivant : `/includes/Utilisateur.inc.php`
---
*   Une classe de base de données à été implémenté dans le but pur et simple de simplifier les requêtes SQL. La réécriture des requêtes SQL donne l'impression actuel de lire un livre qui donne des ordre naturellement. Celle-ci permet aussi d'éviter les erreurs de syntaxes liées aux requêtes elle-même et de limiter la longueur d'une requête assez compliquée pour un humain sans analyse de celle-ci.

*   Certaines requêtes (notamment ceux qui sont compliqué comme l'affichage des meilleurs score) n'utilisent pas l'API qui simplifie la lecture de la requête. Ce qui induit la préparation des requêtes SQL sauvage en plein milieux des fichiers php principale servant à l'affichage utilisateur. Nous voulons ainsi limiter le temps engendrée par l'adaptation de la requête SQL.

Les layers de la carte de leaflet (countries) proviennent d'une source de donnée libre de droit disponible à cette adresse ..

Les drapeaux aux formats SVG proviennent d'une source de donnée libre de droit situé à cette adresse :

La codification des drapeaux sont faits au format ```ALPHA-2``` afin d'éviter une longue tâche d'adaptation de la codification des layers des pays.

Pour le quiz avec les drapeaux, il est plus intuitif d'utiliser les layers des frontières en tant que bouton d'interaction. Celle-ci permet de bien distinguer les frontières des uns des autres.

Les évènements liés aux cliques de la map sont gérés par la librairie SweetAlert. La fonction de sweet-alert est composé d'un argument qui est renvoyé par le layer cliqué contenant le code ALPHA-2 du pays.

### Front-end ###

Afin de donnée l'impression d'un aspect moderne lié à l'intéraction du site, il a été décidé de remplacer l'intégralités des alertes javascript par une librarie SweetAlert2. On est conscient que ceci peut surcharger la mémoire RAM du navigateur web utilisé. Nos arguments sont les suivantes :
-   L'innovation rapide de la technologie amène à l'augmentation de la mémoire RAM disponible suite à un renouvellement rapide du matériel informatique.
-   Selon les objectifs du client, l'attractivité visuelle du site restera un point important. En effet, plus le visiteur restera actif sur la durée dès la première visite, plus ce visiteur sera succetible d'être un visiteur régulier (c'est le phénomène de conversion d'un client potentiel en client régulier).

On peut cependant noter que l'utilisation d'un layer GeoJSON peut avoir des conséquences au chargement du quizz suite au poids du fichier assez conséquent.

### Présentation et mise en place détaillée du site ###

Le site est fait pour être utilisé par l'intermédaire d'un serveur web de type apache php et sql.
La configuration au moment de l'écriture de ce rapport est :
*   PHP 7.4
*   MariaDB 10.4.10
*   Apache 2.4.41
*   phpMyAdmin 4.9.2 (Facultative).

Le site web requiert l'utilisation d'une base de donnée. Un fichier SQL nommée `import.sql` déstinée à l'importation de la base de donnée est fournie. Cette étape est impérative dans le bon fonctionnnement du site (Au risque de voir le message d'erreur concernant les problèmes de connexion à la base de données).

Après l'importation, il faudra configurer l'accès à la base de données en personalisant les identifiants de connexion et le nom de la base de donnée à celle-ci.
Ce fichier se trouve à partir de la racine du site :

```
core/config.php
```

Les champs à modifier selon la configuration sont les suivantes :
```php
$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1', <-
        'username' => 'root',  <-
        'password' => '',      <-
        'DataBase' => 'web'    <-
    ),
```

Le site dispose des nommage conventiels d'un fichier, c'est-à-dire qu'il faudra juste ouvrir le lien du site web à sa racine (localhost/flag_games par exemple). Celle-ci est du à la présence d'un fichier `index.php`

![Index](static/img/index.jpg)

La page d'accueil dispose d'un menu responsive, d'un footer, d'un body, et les boutons pour la connexion, inscription.

L'inscription et la connexion au site sont gérés par les modales SweetAlert, écrite en Javascript.

La vérification pour l'inscription se fait en deux temps :
*   La vérification locale permet de tester si tout les champs sont bien renseignés, et que les mot de passe sont identiques. Celle-ci est la vérification en précondition avant de l'envoyer en back-end php par la méthode `POST`.
*   La vérification backend vérifie la redondance des informations fournies. Si l'utilisateur est déjà présent dans la base de donnée, un message d'erreur est envoyé à l'utilisateur avec une redirection vers l'accueil.

Dans les deux cas (inscription et connexion), l'utilisateur a le choix d'afficher ou cacher son mot de passe.

Pour des raison évidentes de sécurité, le site n'informe pas les informations erronées à la connexion. Elle affichera seulement les informations n'ont pas pu être vérifiés.

![Index Login Hidden](static/img/indexLoginHidded.jpg)
![Index Login Showed](static/img/indexLoginShowed.jpg)
![Index SignUp Error](static/img/indexSignUpNotMatchedjpg.jpg)
![Index Successfull Login](static/img/indexLogInSuccessfull.jpg)
![Index Successfull Failure](static/img/indexLogInFailure.jpg)

L'index du site ressemble à l'image suivante après que l'utilisateur soit connecté.

![Index LoggedIn](static/img/indexLoggedIn.jpg)

Pour un utilisateur connecté, l'utilisateur doit configurer le quiz avant de pouvoir commencer à répondre au question. La configuration du quiz permettera de générer une séries de question en fonction du continent et de la quantité de pays choisie. L'utilisateur est guidée tout au long de la configuration du quiz. La configuration du quiz est alors rendue intuitif.

La configuration du quiz possède des préconditions dans le cas où l'utilisateur saisie des valeurs qui ne serais pas reconnu par le back-end. La deuxième étape de la configuration assure donc que l'utilisateur saisie un nombre strictement positive.

La configuration du quiz s'étale sur deux étapes :
-   La première étape est le choix d'un continent, par l'intermédiaire d'un menu déroulant. La liste reflète la norme ISO sur le nommage des continents.

![Quiz Configuration Step 1](static/img/quizConfigStep1.jpg)

-   La deuxième étape est le choix d'un nombre strictement positive qui déterminera le nombre de question.

![Quiz Configuration Step 2](static/img/quizConfigStep2.jpg)

Un message de confirmation sera présenté à l'utilisateur à la bonne execution de la génération d'un questionnaire avec la configuration donnée.

![Quiz Configuration Step 3](static/img/quizConfigStep3.jpg)

Le site web met à disposition de l'utilisateur d'un guide d'utilisation du quiz. Ce guide est identifié par le bouton `Need help to get started ?`. Ce bouton de couleur bleu se trouve en bas à gauche site.

![Quiz Help](static/img/quizHelp.jpg)

Lorsque l'utilisateur choisie une bonne réponse, le site lui informeras de la bonne réponse et lui proposera d'ouvrir une fenêtre pop-up d'une page Wikipédia dans le but de se cultiver. Le site proposera à l'utilisateur d'ouvrir une fenêtre pop-up pour avoir des images du pays en question.

![Quiz reply correct](static/img/quizReplyCorrect1.jpg)

En revanche, lorsque l'utilisateur choisie une mauvaise réponse, le site va informer l'utilisateur de la mauvaise réponse, et le nombre de coups restante avant de dévoiler le nom du pays.
![Quiz reply failure](static/img/QuizReplyFailure.jpg)

A la fin du quiz, le site lui informera du score final de l'utilisateur et lui demandera s'il veut sauvegarder vers la base de données des meilleurs scores ou non. Le site lui donne aussi la possibilité de rejouer. Dans ce cas, l'utilisateur devra donc reconfigurer le quiz.

![Quiz finished](static/img/QuizFinished.jpg)

Le site offre la possibilité à tout le monde de consulter la liste des meilleurs score.

![Best Scores](static/img/BestScores.jpg)

L'utilisateur s'il le souhaite, peut procéder à la rectification de ses informations qui lui est associés à partir de son espace dédiée nommée `My account`

![Account Management](static/img/AccountManagement.jpg)

Les informations qui peuvent être rectifés sont les suivantes :
*   Mot de passe

## Conclusion ##

Au travers de ce projet, nous avons réussi à créer un jeu interactif à but éducatif. Les questionaires sont imprévisible puisqu'ils sont générés aléatoirement. La modularitée du projet est importante suite à l'implémentation en orientée objet. Actuellement le jeu
est fonctionnel mais nous aurions pu suivre quelques pistes d’amélioration qui sont les suivantes :
-   Proposer un quiz sur le placement d'une capitale d'un pays.
-   Un panel d'administration fonctionnel, nous avons juste un squelette qui n'est pas fonctionnel.
-   Moduler les classement de meilleurs scores sur des critères tels que le nombre de question, sur quels continents.

Le jeu est néanmoins fonctionnel.
