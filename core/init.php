<?php
//echo ROOT_PATH;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// SQL configuration
$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'DataBase' => 'web'
    ),
    'remember' => array(
        'cookie_name' => 'hash',
        'cookie_expiry' => 604800
    ),
    'sessions' => array(
        'session_name' => 'user',
        'token_name' => 'token'
    )
);

// Autoloader class

spl_autoload_register(function($class) {
    require_once (__DIR__.'/../classes/' . $class . '.class.php');
});

require_once (__DIR__.'/../functions/sanitize.php');


?>