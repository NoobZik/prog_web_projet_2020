<?php


class Countries {
    public $name;
    public $continent;
    public $capital;
    public $alpha2;
    public $alpha3;

    public function __construct($name, $continent, $capital, $alpha2, $alpha3) {
        $this->name = $name;
        $this->continent = $continent;
        $this->capital = $capital;
        $this->alpha2 = $alpha2;
        $this->alpha3 = $alpha3;
    }

    /**
     * @return mixed
     */
    public function getContinent() {
        return $this->continent;
    }

    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCapital() {
        return $this->capital;
    }

    /**
     * @return string
     */
    public function getAlpha2() {
        return $this->alpha2;
    }

    /**
     * @return string
     */
    public function getAlpha3() {
        return $this->alpha3;
    }
}