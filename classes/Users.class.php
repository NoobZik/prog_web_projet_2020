<?php

class Users  {
    private $userID;
    private $username;
    private $password;
    private $mail;
    private $date;


    public function __construct() {
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if (method_exists($this, $function = '__construct'.$numberOfArguments)) {
            call_user_func_array(array($this, $function), $arguments);
        }
    }

    public function __construct1($name) {
        $this->username = $name;
    }

    public function __construct3($name, $password, $mail) {
        $this->username = $name;
        $this->password = $password;
        $this->mail = $mail;
    }

    public function __construct4($name, $password, $mail, $userID) {
        $this->username = $name;
        $this->password = $password;
        $this->mail = $mail;
        $this->userID = $userID;
    }

    public function __construct5($name, $password, $mail, $userID, $date) {
        $this->username = $name;
        $this->password = $password;
        $this->mail = $mail;
        $this->userID = $userID;
        $this->date = strtotime($date);

    }

    /**
     * Retrieve an User from it username and return it result
     *
     * @param $name
     * @return Users|null
     */
    public function getUser($name) {
        $results = DataBase::getInstance()->get('users', array('username', '=', $name));
        if ($results->count()) {
            $user = $results->first();
            return new Users($user->username, $user->password, $user->mail, $user->id, $user->date_signUp);
        }
        else {
            return null;
        }

    }

    /**
     * Create a user into the database
     * If the is user already in the database, operation is stopped
     *
     *
     * @return  boolean             [return description]
     */
    public function setUser() {
        // Check if the user exists
        $result = $this->getUser($this->getUsername());
        if ($result != null) {
            return false;
        }

        // Search for duplicate mail here

        $result = DataBase::getInstance()->get('users', array('mail', '=', $this->mail));
        if ($result != null) {
            return false;
        }
        $date = new DateTime('now');
        $result = DataBase::getInstance()->insert('users',array(
            'username' => $this->getUsername(),
            'password' => $this->hashPassword($this->password),
            'mail'     => $this->getMail(),
            'date_signUp'     => $date->format('Y-m-d')
        ));
        return ($result) ? true : false;
    }

     protected function hashPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function getPassword() {
        return $this->password;
    }

    /**
     * A finir
     *
     * @param   [type]  $password  [$password description]
     *
     * @return  boolean             [return description]
     */
    public function verifyPassword($test) {
        //$result = DataBase::getInstance()->get('users', array('username', '=', $this->username));
        $user = $this->getUser($this->getUsername());
        if ($user == null) {
            return false;
        }
        return (password_verify($test, $user->getPassword())) ? true : false;
    }

    /**
     * @return bool
     */
    public function chechLoginStatus() {
        return (isset($_SESSION["loggedin"])) ? true : false;
    }

    /**
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getMail() {
        return $this->mail;
    }

    /**
     * @param $newPassword
     * @return bool
     */
    public function setPasswordAndUpdate($newPassword) {
        $this->password = $this->hashPassword($newPassword);
        $sql = 'UPDATE users SET password ="' . $this->password . '"WHERE id = ' . $this->getUserID();
        $result = DataBase::getInstance();
        $result = $result->query($sql, array());
        return !$result->error();
    }

    /**
     * @return mixed
     */
    public function getUserID() {
        return $this->userID;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

}

?>