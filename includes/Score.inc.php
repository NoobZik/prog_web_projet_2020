<?php

include (__DIR__.'/../core/init.php');

$date = new DateTime('now');

/**
 * If a score is provided, insert the score into the database
 * -1 Is for non-registered users
 * Any other positive number are registered users already logged in.
 */

if (isset($_POST["score"])) {
    if (isset($_SESSION['username'])) {
        if (!isset($_SESSION['userID'])) {
            echo "Error";
            die();
        }
         $db = DataBase::getInstance();
         $result = $db->insert('score',array(
                "userID" => $_SESSION['userID'],
                "score" => $_POST["score"],
                "date" => $date->format('Y-m-d')
            ));
         if ($result)
            echo 'Successfully added to the score wall !';
         else
             echo 'Error while processing to the database';
    }
    else {
        $db = DataBase::getInstance();
        $result = $db->insert('score', array(
            "userID" => -1,
            "score" => $_POST["score"],
            "date" => $date->format('Y-m-d')
        ));
        if ($result)
            echo 'Successfully added to the score wall !';
        else
            echo 'Error while processing to the database';
    }
}

else {
    echo json_encode("Nothing received");

}