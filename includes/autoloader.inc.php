<?php

spl_autoload_register("autoLoad");

/**
 * Autoload a class when requested
 */

function autoLoad($className) {
    $path = __DIR__."/../classes/";
    $extension = ".class.php";
    $fullPath = $path . $className . $extension;

    if (!file_exists($fullPath)) {
        return false;
    }

    include_once $fullPath;
    return true;
}

?>