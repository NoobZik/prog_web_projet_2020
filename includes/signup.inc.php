<?php


include (__DIR__.'/../core/init.php');
/**
 * If a form is provided
 * Register the user by creating a SQL query
 */
if (isset($_POST['username_reg']) && isset($_POST['password1']) && isset($_POST['mail'])) {
    $username = $_POST['username_reg'];
    $password = $_POST['password1'];
    $mail= $_POST['mail'];

    $user = new Users($username, $password, $mail);
    if ($user->setUser() !== false) {
        echo 'Successfully registered';
        $_SESSION["registrationSuccess"] = true;
        header('Refresh:0; URL=../index.php');

    }
    else {
        echo 'Error, maybe already registered';
        $_SESSION["registrationFailure"] = true;
        header('Refresh:0; URL=../index.php');
    }
}
else {
    echo 'No forms provided';
}
