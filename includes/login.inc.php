<?php
//include "autoloader.inc.php";
require (__DIR__.'/../core/init.php');
//var_dump($_POST);

/**
 * If a username and a password is provided from a form
 * Performs the matching process
 * Store the loggin details if the matches succeed.
 *
 * If it does not, return to the index with a log in error message
 */
if (isset($_POST['username']) && $_POST['password']) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $user = new Users($username, $password, null);
    if ($user->verifyPassword($password)) {
        //echo 'Credentials  verified<br>';
        $_SESSION["username"] = $user->getUsername();
        $user = $user->getUser($_SESSION["username"]);
        $_SESSION["email"] = $user->getMail();
        $_SESSION["userID"] = $user->getUserID();
        $_SESSION["loggedIn"] = true;
        $_SESSION["signUpDate"] = $user->getDate();
        header('Refresh:0; URL=../index.php');
    }
    else {
        //echo 'Credentials verification failure<br>';
        $_SESSION["logInError"] = true;
        header('Refresh:0; URL=../index.php');

    }
}
/**
 * Changing password process for account management.
 */
else if (isset($_POST['oldPassword'])) {
  //  $user = $_SESSION["user"];
  //  echo $_SESSION["username"];
    $user = new Users($_SESSION["username"]);
    $user = $user->getUser($user->getUsername());
    $oldPassword = $_POST['oldPassword'];
    echo json_encode($user->verifyPassword($oldPassword));
    unset($_POST['oldPassword']);

}


else if (isset($_POST['newPassword'])) {
    $user = new Users($_SESSION["username"]);
    $user = $user->getUser($user->getUsername());
    echo json_encode($user->setPasswordAndUpdate($_POST['newPassword']));
    unset($_POST['newPassword']);
}

?>