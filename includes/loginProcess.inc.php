<script src="js/swal_customs.js"></script>

<?php

include_once (__DIR__."/../core/init.php");

/**
 * Display all kinds of messages regarding the login process
 */

if (isset($_SESSION["logOut"])) {
    echo '<script type="text/javascript"> logOut(); </script>';
    unset($_SESSION["logOut"]);
}
else if (isset($_SESSION["loggedIn"])) {
    echo '<script type="text/javascript"> loggedIn(); </script>';
    unset($_SESSION["loggedIn"]);
}
else if (isset($_SESSION["logInError"])) {
    echo '<script type="text/javascript"> loginError(); </script>';
    unset($_SESSION["logInError"]);
}
else if (isset($_SESSION["registrationSuccess"])) {
    echo '<script type="text/javascript"> registrationSuccess(); </script>';
    unset($_SESSION["registrationSuccess"]);
}
else if (isset($_SESSION["registrationFailure"])) {
    echo '<script type="text/javascript"> registrationFailure(); </script>';
    unset($_SESSION["registrationFailure"]);
}
