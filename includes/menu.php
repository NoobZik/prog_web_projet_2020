<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">GeoNav</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor03">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="flag_finder.php">Flag Quiz</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="scores.php">Best Scores</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="about.php">About</a>
            </li>
        </ul>
        <ul class="nav navbar-nav">

        <?php
        if (!isset($_SESSION["username"])) {
            ?>
            <button type="button" class="btn btn-info mx-1" onclick='login()'>Login</button>
            <button type="button" class="btn btn-info mx-1" onclick='signUp()'>Sign Up</button>
            <?php
        }
        else {
            ?>
            <li class="navbar-text" >Welcome <?php echo $_SESSION["username"]; ?></li>
            <button type="button" class="btn btn-info btn-margin-left mx-1" onclick="window.location.href = 'accountManagement.php';">My Account</button>
            <button type="button" class="btn btn-danger btn-margin-left mx-1" onclick="window.location.href = 'includes/logout.inc.php';">Log out</button>
        <?php
        }
        ?>
        </ul>

    </div>
</nav>