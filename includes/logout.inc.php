<?php
include (__DIR__.'/../core/init.php');
/**
 * Unset any logging details variable and destroys the session.
 */
unset($_SESSION["userID"]);
unset($_SESSION["username"]);
unset($_SESSION["email"]);
session_destroy();
session_start();
$_SESSION["logOut"] = true;
header('Refresh:0; URL=../index.php');
?>