<?php
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   about.php                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 00:33:36 by noobzik           #+#    #+#             */
/*   Updated: 2020/03/05 00:33:36 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


//include 'includes/autoloader.inc.php';

require_once (__DIR__.'/core/init.php');


?>

<!DOCTYPE HTML>
<html lang="fr" class="h-100">
    <head>
        <title>PROJECT OXYGEN : FAC</title>
        <meta charset="utf-8" />
		<meta name="description" content="Le jeu des capitales teste vos compétences en géographie pour retrouver des pays et leurs capitales sur une carte" />
		<meta name="keywords" content="jeu capitales géographie geo pays carte monde europe afrique etats unis oceanie australie points score">
		<meta name="viewport" content="width=device-width, initial-scale=1" />

        <script src="js/jQuery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="js/SweetAlert2.js"></script>
        <script src="js/swal_customs.js"></script>
        <script src="js/bootstrap.bundle.js"></script>


        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/SweetAlert2.css" />
        <link rel="stylesheet" href="css/flatty.css" />
        <link rel="stylesheet" href="css/style.css" />

        <!--script src="js/jquery.js"></script-->
    </head>
    <body class="d-flex flex-column h-100">
    <?php include "includes/menu.php";?>
        <div class="container">
            <?php
            include_once (__DIR__."/includes/loginProcess.inc.php");
            ?>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <blockquote class="blockquote">
                            <p class="mb-0">About : GeoNav quizz</p>
                            <footer class="blockquote-footer">Learning places all around the world has never been so easy!</footer>
                        </blockquote>
                        <p>This game is developed by Rakib Sheikh (nickname : NoobZik) and Sohayla Rahbi (Sohayla5) in order to fulfil a project assignment from the University Sorbonne Paris Nord.</p>
                        <p>The project is using the following languages and libraries</p>
                        <list>
                            <ul>HTML5, CSS, Javascript, PHP</ul>
                            <ul>MariaDB</ul>
                            <ul>Bootstrap 4</ul>
                            <ul>SweetAlert 2</ul>
                            <ul>Leaflet</ul>
                            <ul>Ajax</ul>
                            <ul>jQuery</ul>
                        </list>
                        <p>
                            This project ensure that all data collected from the user are destroyed within a certain time which is set by the server (PHP Session timeout for references).<br />
                            Only the following data :
                        </p>
                        <list>
                            <ul>Username</ul>
                            <ul>Password</ul>
                            <ul>Email</ul>
                            <ul>Scores</ul>
                        </list>
                        <p>Are stored into an SQL Database (assuming MariaDB)</p>
                    </div>
                </div>
            </div>
        </div>
    <?php include(__DIR__."/includes/footer.inc.php"); ?>

    </body>


</html>



