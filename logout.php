<?php

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logout.php                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: noobzik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 00:13:51 by noobzik           #+#    #+#             */
/*   Updated: 2020/03/05 00:13:51 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

    session_start();
    session_destroy();
?>