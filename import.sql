-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 09, 2020 at 11:37 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web`
--

-- --------------------------------------------------------

--
-- Table structure for table `continents`
--

DROP TABLE IF EXISTS `continents`;
CREATE TABLE IF NOT EXISTS `continents` (
  `code` char(2) NOT NULL COMMENT 'Continent code',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `continents`
--

INSERT INTO `continents` (`code`, `name`) VALUES
('AF', 'Africa'),
('AN', 'Antarctica'),
('AS', 'Asia'),
('EU', 'Europe'),
('NA', 'North America'),
('OC', 'Oceania'),
('SA', 'South America');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `code` char(2) NOT NULL COMMENT 'Two-letter country code (ISO 3166-1 alpha-2)',
  `name` varchar(255) NOT NULL COMMENT 'English country name',
  `full_name` varchar(255) NOT NULL COMMENT 'Full English country name',
  `iso3` char(3) NOT NULL COMMENT 'Three-letter country code (ISO 3166-1 alpha-3)',
  `number` char(3) NOT NULL COMMENT 'Three-digit country number (ISO 3166-1 numeric)',
  `continent_code` char(2) NOT NULL,
  `capital` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`code`),
  KEY `continent_code` (`continent_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`code`, `name`, `full_name`, `iso3`, `number`, `continent_code`, `capital`) VALUES
('AD', 'Andorra', 'Principality of Andorra', 'AND', '020', 'EU', 'Andorra la Villa'),
('AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', 'AS', 'Abu Dhabi'),
('AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', 'AS', 'Kabul'),
('AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', 'NA', 'Saint John\'s'),
('AI', 'Anguilla', 'Anguilla', 'AIA', '660', 'NA', 'The Valley'),
('AL', 'Albania', 'Republic of Albania', 'ALB', '008', 'EU', 'Tirana'),
('AM', 'Armenia', 'Republic of Armenia', 'ARM', '051', 'AS', 'Yerevan'),
('AO', 'Angola', 'Republic of Angola', 'AGO', '024', 'AF', 'Luanda'),
('AQ', 'Antarctica', 'Antarctica (the territory South of 60 deg S)', 'ATA', '010', 'AN', NULL),
('AR', 'Argentina', 'Argentine Republic', 'ARG', '032', 'SA', 'Buenos Aires'),
('AS', 'American Samoa', 'American Samoa', 'ASM', '016', 'OC', 'Pago Pago'),
('AT', 'Austria', 'Republic of Austria', 'AUT', '040', 'EU', 'Vienna'),
('AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', 'OC', 'Canberra'),
('AW', 'Aruba', 'Aruba', 'ABW', '533', 'NA', 'Oranjestad'),
('AX', 'Åland Islands', 'Åland Islands', 'ALA', '248', 'EU', 'Mariehamn'),
('AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', '031', 'AS', 'Baku'),
('BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', '070', 'EU', 'Sarajevo'),
('BB', 'Barbados', 'Barbados', 'BRB', '052', 'NA', 'Bridgetown'),
('BD', 'Bangladesh', 'People\'s Republic of Bangladesh', 'BGD', '050', 'AS', 'Dhaka'),
('BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', 'EU', 'Brussels'),
('BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', 'AF', 'Ouagadougou'),
('BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', '100', 'EU', 'Sofia'),
('BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', '048', 'AS', 'Manama'),
('BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', 'AF', 'Bujumbura'),
('BJ', 'Benin', 'Republic of Benin', 'BEN', '204', 'AF', 'Porto-Novo'),
('BL', 'Saint Barthélemy', 'Saint Barthélemy', 'BLM', '652', 'NA', 'Gustavia'),
('BM', 'Bermuda', 'Bermuda', 'BMU', '060', 'NA', 'Hamilton'),
('BN', 'Brunei Darussalam', 'Brunei Darussalam', 'BRN', '096', 'AS', 'Bandar Seri Begawan'),
('BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', 'SA', 'La Paz'),
('BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', 'NA', NULL),
('BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', 'SA', 'Brasilia'),
('BS', 'Bahamas', 'Commonwealth of the Bahamas', 'BHS', '044', 'NA', 'Nassau'),
('BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', 'AS', 'Thimphu'),
('BV', 'Bouvet Island (Bouvetøya)', 'Bouvet Island (Bouvetøya)', 'BVT', '074', 'AN', NULL),
('BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', 'AF', 'Gaborone'),
('BY', 'Belarus', 'Republic of Belarus', 'BLR', '112', 'EU', 'Minsk'),
('BZ', 'Belize', 'Belize', 'BLZ', '084', 'NA', 'Belmopan'),
('CA', 'Canada', 'Canada', 'CAN', '124', 'NA', 'Ottawa'),
('CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', 'AS', 'West Island'),
('CD', 'Congo', 'Democratic Republic of the Congo', 'COD', '180', 'AF', 'Kinshasa'),
('CF', 'Central African Republic', 'Central African Republic', 'CAF', '140', 'AF', 'Bangui'),
('CG', 'Congo', 'Republic of the Congo', 'COG', '178', 'AF', 'Brazzaville'),
('CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', 'EU', 'Bern'),
('CI', 'Cote d\'Ivoire', 'Republic of Cote d\'Ivoire', 'CIV', '384', 'AF', 'Yamoussoukro'),
('CK', 'Cook Islands', 'Cook Islands', 'COK', '184', 'OC', 'Avarua'),
('CL', 'Chile', 'Republic of Chile', 'CHL', '152', 'SA', 'Santiago'),
('CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', 'AF', 'Yaounde'),
('CN', 'China', 'People\'s Republic of China', 'CHN', '156', 'AS', 'Beijing'),
('CO', 'Colombia', 'Republic of Colombia', 'COL', '170', 'SA', 'Bogota'),
('CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', 'NA', 'San Jose'),
('CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', 'NA', 'Havana'),
('CV', 'Cabo Verde', 'Republic of Cabo Verde', 'CPV', '132', 'AF', 'Praia'),
('CW', 'Curaçao', 'Curaçao', 'CUW', '531', 'NA', 'Willemstad'),
('CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', 'AS', 'The Settlement'),
('CY', 'Cyprus', 'Republic of Cyprus', 'CYP', '196', 'AS', 'Nicosia'),
('CZ', 'Czechia', 'Czech Republic', 'CZE', '203', 'EU', 'Prague'),
('DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', 'EU', 'Berlin'),
('DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', 'AF', 'Djibouti'),
('DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', 'EU', 'Copenhagen'),
('DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', 'NA', 'Roseau'),
('DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', 'NA', 'Santo Domingo'),
('DZ', 'Algeria', 'People\'s Democratic Republic of Algeria', 'DZA', '012', 'AF', 'Algiers'),
('EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', 'SA', 'Quito'),
('EE', 'Estonia', 'Republic of Estonia', 'EST', '233', 'EU', 'Tallinn'),
('EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', '818', 'AF', 'Cairo'),
('EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', 'AF', 'Laayoune'),
('ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', 'AF', 'Asmara'),
('ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', 'EU', 'Madrid'),
('ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', 'AF', 'Addis Ababa'),
('FI', 'Finland', 'Republic of Finland', 'FIN', '246', 'EU', 'Helsinki'),
('FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', 'OC', 'Suva'),
('FK', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', 'FLK', '238', 'SA', 'Stanley'),
('FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', 'OC', 'Palikir'),
('FO', 'Faroe Islands', 'Faroe Islands', 'FRO', '234', 'EU', 'Torshavn'),
('FR', 'France', 'French Republic', 'FRA', '250', 'EU', 'Paris'),
('GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', 'AF', 'Libreville'),
('GB', 'United Kingdom of Great Britain and Northern Ireland', 'United Kingdom of Great Britain & Northern Ireland', 'GBR', '826', 'EU', 'London'),
('GD', 'Grenada', 'Grenada', 'GRD', '308', 'NA', 'Saint George’s'),
('GE', 'Georgia', 'Georgia', 'GEO', '268', 'AS', 'Tbilisi'),
('GF', 'French Guiana', 'French Guiana', 'GUF', '254', 'SA', 'Cayenne'),
('GG', 'Guernsey', 'Bailiwick of Guernsey', 'GGY', '831', 'EU', 'Saint Peter Port'),
('GH', 'Ghana', 'Republic of Ghana', 'GHA', '288', 'AF', 'Accra'),
('GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', 'EU', 'Gibraltar'),
('GL', 'Greenland', 'Greenland', 'GRL', '304', 'NA', 'Nuuk'),
('GM', 'Gambia', 'Republic of the Gambia', 'GMB', '270', 'AF', 'Banjul'),
('GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', 'AF', 'Conakry'),
('GP', 'Guadeloupe', 'Guadeloupe', 'GLP', '312', 'NA', 'Basse-Terre'),
('GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', 'AF', 'Malabo'),
('GR', 'Greece', 'Hellenic Republic of Greece', 'GRC', '300', 'EU', 'Athens'),
('GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', '239', 'AN', 'King Edward Point'),
('GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', 'NA', 'Guatemala City'),
('GU', 'Guam', 'Guam', 'GUM', '316', 'OC', 'Hagatna'),
('GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', 'AF', 'Bissau'),
('GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', 'SA', 'Georgetown'),
('HK', 'Hong Kong', 'Hong Kong Special Administrative Region of China', 'HKG', '344', 'AS', NULL),
('HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'AN', NULL),
('HN', 'Honduras', 'Republic of Honduras', 'HND', '340', 'NA', 'Tegucigalpa'),
('HR', 'Croatia', 'Republic of Croatia', 'HRV', '191', 'EU', 'Zagreb'),
('HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', 'NA', 'Port-au-Prince'),
('HU', 'Hungary', 'Hungary', 'HUN', '348', 'EU', 'Budapest'),
('ID', 'Indonesia', 'Republic of Indonesia', 'IDN', '360', 'AS', 'Jakarta'),
('IE', 'Ireland', 'Ireland', 'IRL', '372', 'EU', 'Dublin'),
('IL', 'Israel', 'State of Israel', 'ISR', '376', 'AS', 'Jerusalem'),
('IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', 'EU', 'Douglas'),
('IN', 'India', 'Republic of India', 'IND', '356', 'AS', 'New Delhi'),
('IO', 'British Indian Ocean Territory (Chagos Archipelago)', 'British Indian Ocean Territory (Chagos Archipelago)', 'IOT', '086', 'AS', 'Eclipse Point Town'),
('IQ', 'Iraq', 'Republic of Iraq', 'IRQ', '368', 'AS', 'Baghdad'),
('IR', 'Iran', 'Islamic Republic of Iran', 'IRN', '364', 'AS', 'Tehran'),
('IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', 'EU', 'Reykjavik'),
('IT', 'Italy', 'Republic of Italy', 'ITA', '380', 'EU', 'Rome'),
('JE', 'Jersey', 'Bailiwick of Jersey', 'JEY', '832', 'EU', 'Saint Helier'),
('JM', 'Jamaica', 'Jamaica', 'JAM', '388', 'NA', 'Kingston'),
('JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', '400', 'AS', 'Amman'),
('JP', 'Japan', 'Japan', 'JPN', '392', 'AS', 'Tokyo'),
('KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', 'AF', 'Nairobi'),
('KG', 'Kyrgyz Republic', 'Kyrgyz Republic', 'KGZ', '417', 'AS', 'Bishkek'),
('KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', 'AS', 'Phnom Penh'),
('KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', 'OC', 'Tarawa'),
('KM', 'Comoros', 'Union of the Comoros', 'COM', '174', 'AF', 'Moroni'),
('KN', 'Saint Kitts and Nevis', 'Federation of Saint Kitts and Nevis', 'KNA', '659', 'NA', 'Basseterre'),
('KP', 'Korea', 'Democratic People\'s Republic of Korea', 'PRK', '408', 'AS', 'Pyongyang'),
('KR', 'Korea', 'Republic of Korea', 'KOR', '410', 'AS', 'Seoul'),
('KW', 'Kuwait', 'State of Kuwait', 'KWT', '414', 'AS', 'Kuwait City'),
('KY', 'Cayman Islands', 'Cayman Islands', 'CYM', '136', 'NA', 'George Town'),
('KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', 'AS', 'Astana'),
('LA', 'Lao People\'s Democratic Republic', 'Lao People\'s Democratic Republic', 'LAO', '418', 'AS', 'Vientiane'),
('LB', 'Lebanon', 'Lebanese Republic', 'LBN', '422', 'AS', 'Beirut'),
('LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', 'NA', 'Castries'),
('LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', 'EU', 'Vaduz'),
('LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', 'AS', 'Colombo'),
('LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', 'AF', 'Monrovia'),
('LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', 'AF', 'Maseru'),
('LT', 'Lithuania', 'Republic of Lithuania', 'LTU', '440', 'EU', 'Vilnius'),
('LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', 'EU', 'Luxembourg'),
('LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', 'EU', 'Riga'),
('LY', 'Libya', 'State of Libya', 'LBY', '434', 'AF', 'Tripoli'),
('MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', 'AF', 'Rabat'),
('MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', 'EU', 'Monaco'),
('MD', 'Moldova', 'Republic of Moldova', 'MDA', '498', 'EU', 'Chisinau'),
('ME', 'Montenegro', 'Montenegro', 'MNE', '499', 'EU', 'Podgorica'),
('MF', 'Saint Martin', 'Saint Martin (French part)', 'MAF', '663', 'NA', 'Marigot'),
('MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', 'AF', 'Antananarivo'),
('MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', 'OC', 'Majuro'),
('MK', 'North Macedonia', 'Republic of North Macedonia', 'MKD', '807', 'EU', 'Skopje'),
('ML', 'Mali', 'Republic of Mali', 'MLI', '466', 'AF', 'Bamako'),
('MM', 'Myanmar', 'Republic of the Union of Myanmar', 'MMR', '104', 'AS', 'Rangoon'),
('MN', 'Mongolia', 'Mongolia', 'MNG', '496', 'AS', 'Ulaanbaatar'),
('MO', 'Macao', 'Macao Special Administrative Region of China', 'MAC', '446', 'AS', NULL),
('MP', 'Northern Mariana Islands', 'Commonwealth of the Northern Mariana Islands', 'MNP', '580', 'OC', 'Saipan'),
('MQ', 'Martinique', 'Martinique', 'MTQ', '474', 'NA', 'Fort-de-France'),
('MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', 'AF', 'Nouakchott'),
('MS', 'Montserrat', 'Montserrat', 'MSR', '500', 'NA', 'Plymouth'),
('MT', 'Malta', 'Republic of Malta', 'MLT', '470', 'EU', 'Valletta'),
('MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', 'AF', 'Port Louis'),
('MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', 'AS', 'Male'),
('MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', 'AF', 'Lilongwe'),
('MX', 'Mexico', 'United Mexican States', 'MEX', '484', 'NA', 'Mexico City'),
('MY', 'Malaysia', 'Malaysia', 'MYS', '458', 'AS', 'Kuala Lumpur'),
('MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', 'AF', 'Maputo'),
('NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', 'AF', 'Windhoek'),
('NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', 'OC', 'Noumea'),
('NE', 'Niger', 'Republic of Niger', 'NER', '562', 'AF', 'Niamey'),
('NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', 'OC', 'Kingston'),
('NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', '566', 'AF', 'Abuja'),
('NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', 'NA', 'Managua'),
('NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', 'EU', 'Amsterdam'),
('NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', 'EU', 'Oslo'),
('NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', 'AS', 'Kathmandu'),
('NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', 'OC', 'Yaren'),
('NU', 'Niue', 'Niue', 'NIU', '570', 'OC', 'Alofi'),
('NZ', 'New Zealand', 'New Zealand', 'NZL', '554', 'OC', 'Wellington'),
('OM', 'Oman', 'Sultanate of Oman', 'OMN', '512', 'AS', 'Muscat'),
('PA', 'Panama', 'Republic of Panama', 'PAN', '591', 'NA', 'Panama City'),
('PE', 'Peru', 'Republic of Peru', 'PER', '604', 'SA', 'Lima'),
('PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', 'OC', 'Papeete'),
('PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', 'OC', 'Port Moresby'),
('PH', 'Philippines', 'Republic of the Philippines', 'PHL', '608', 'AS', 'Manila'),
('PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', '586', 'AS', 'Islamabad'),
('PL', 'Poland', 'Republic of Poland', 'POL', '616', 'EU', 'Warsaw'),
('PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', 'NA', 'Saint-Pierre'),
('PN', 'Pitcairn Islands', 'Pitcairn Islands', 'PCN', '612', 'OC', 'Adamstown'),
('PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', 'NA', 'San Juan'),
('PS', 'Palestine', 'State of Palestine', 'PSE', '275', 'AS', 'Jerusalem'),
('PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', 'EU', 'Lisbon'),
('PW', 'Palau', 'Republic of Palau', 'PLW', '585', 'OC', 'Melekeok'),
('PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', 'SA', 'Asuncion'),
('QA', 'Qatar', 'State of Qatar', 'QAT', '634', 'AS', 'Doha'),
('RE', 'Réunion', 'Réunion', 'REU', '638', 'AF', 'Saint-Denis'),
('RO', 'Romania', 'Romania', 'ROU', '642', 'EU', 'Bucharest'),
('RS', 'Serbia', 'Republic of Serbia', 'SRB', '688', 'EU', 'Belgrade'),
('RU', 'Russian Federation', 'Russian Federation', 'RUS', '643', 'EU', 'Moscow'),
('RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', 'AF', 'Kigali'),
('SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', '682', 'AS', 'Riyadh'),
('SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', 'OC', 'Honiara'),
('SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', 'AF', 'Victoria'),
('SD', 'Sudan', 'Republic of Sudan', 'SDN', '729', 'AF', 'Khartoum'),
('SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', 'EU', 'Stockholm'),
('SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', 'AS', 'Singapore'),
('SH', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', 'AF', 'Jamestown'),
('SI', 'Slovenia', 'Republic of Slovenia', 'SVN', '705', 'EU', 'Ljubljana'),
('SJ', 'Svalbard & Jan Mayen Islands', 'Svalbard & Jan Mayen Islands', 'SJM', '744', 'EU', 'Longyearbyen'),
('SK', 'Slovakia (Slovak Republic)', 'Slovakia (Slovak Republic)', 'SVK', '703', 'EU', 'Bratislava'),
('SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', 'AF', 'Freetown'),
('SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', 'EU', 'San Marino'),
('SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', 'AF', 'Dakar'),
('SO', 'Somalia', 'Federal Republic of Somalia', 'SOM', '706', 'AF', 'Mogadishu'),
('SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', 'SA', 'Paramaribo'),
('SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', 'AF', 'Juba'),
('ST', 'Sao Tome and Principe', 'Democratic Republic of Sao Tome and Principe', 'STP', '678', 'AF', 'Sao Tome'),
('SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', 'NA', 'San Salvador'),
('SX', 'Sint Maarten (Dutch part)', 'Sint Maarten (Dutch part)', 'SXM', '534', 'NA', 'Philipsburg'),
('SY', 'Syrian Arab Republic', 'Syrian Arab Republic', 'SYR', '760', 'AS', 'Damascus'),
('SZ', 'Eswatini', 'Kingdom of Eswatini', 'SWZ', '748', 'AF', 'Mbabane'),
('TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', 'NA', 'Grand Turk'),
('TD', 'Chad', 'Republic of Chad', 'TCD', '148', 'AF', 'N’Djamena'),
('TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', 'AN', 'Saint Pierre'),
('TG', 'Togo', 'Togolese Republic', 'TGO', '768', 'AF', 'Lome'),
('TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', 'AS', 'Bangkok'),
('TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', 'AS', 'Dushanbe'),
('TK', 'Tokelau', 'Tokelau', 'TKL', '772', 'OC', 'Atafu'),
('TL', 'Timor-Leste', 'Democratic Republic of Timor-Leste', 'TLS', '626', 'AS', 'Dili'),
('TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', 'AS', 'Ashgabat'),
('TN', 'Tunisia', 'Tunisian Republic', 'TUN', '788', 'AF', 'Tunis'),
('TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', 'OC', 'Nuku’alofa'),
('TR', 'Turkey', 'Republic of Turkey', 'TUR', '792', 'AS', 'Ankara'),
('TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', 'NA', 'Port of Spain'),
('TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', 'OC', 'Funafuti'),
('TW', 'Taiwan', 'Taiwan, Province of China', 'TWN', '158', 'AS', 'Taipei'),
('TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', 'AF', 'Dar es Salaam'),
('UA', 'Ukraine', 'Ukraine', 'UKR', '804', 'EU', 'Kyiv'),
('UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', 'AF', 'Kampala'),
('UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'OC', 'Washington, D.C.'),
('US', 'United States of America', 'United States of America', 'USA', '840', 'NA', ' 	Washington, D.C.'),
('UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', 'SA', 'Montevideo'),
('UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', 'AS', 'Tashkent'),
('VA', 'Holy See (Vatican City State)', 'Holy See (Vatican City State)', 'VAT', '336', 'EU', 'Vatican City'),
('VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', 'NA', 'Kingstown'),
('VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', 'SA', 'Caracas'),
('VG', 'British Virgin Islands', 'British Virgin Islands', 'VGB', '092', 'NA', 'Road Town'),
('VI', 'United States Virgin Islands', 'United States Virgin Islands', 'VIR', '850', 'NA', 'Charlotte Amalie'),
('VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', 'AS', 'Hanoi'),
('VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', 'OC', 'Port-Vila'),
('WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', 'OC', 'Mata-Utu'),
('WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', 'OC', 'Apia'),
('YE', 'Yemen', 'Yemen', 'YEM', '887', 'AS', 'Sanaa'),
('YT', 'Mayotte', 'Mayotte', 'MYT', '175', 'AF', 'Mamoudzou'),
('ZA', 'South Africa', 'Republic of South Africa', 'ZAF', '710', 'AF', 'Pretoria'),
('ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', 'AF', 'Lusaka'),
('ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', 'AF', 'Harare');

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

DROP TABLE IF EXISTS `score`;
CREATE TABLE IF NOT EXISTS `score` (
  `userID` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `date` date NOT NULL,
  KEY `PK_ETUDIANTS` (`userID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`userID`, `score`, `date`) VALUES
(20, 50, '1990-09-01'),
(18, 9, '2020-06-09'),
(18, 4, '2020-06-09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `date_signUp` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `mail`, `date_signUp`) VALUES
(0, 'public', '0', '0', '0000-00-00'),
(16, 'noobzik', '$2y$10$lBfWr2Qe.BavKM86c2mPruOZr09agEhCWombIa2lvKpOtsidzVhmm', 'noobzik@pm.me', '0000-00-00'),
(18, 'test', '$2y$10$tMQnKp56sOGDX9Koekq0t.6o7t17X3HTMT8Y86QL2dsjOYVYxJXte', 'test@pm.test', '0000-00-00'),
(19, 'test1', '$2y$10$prDYzBO/uAQxB0gOavOop.JyjnDVAHs2vLeGYmPMDtqdOMu/v5xz.', 'test@pm.test', '0000-00-00'),
(20, 'dhdfh', '$2y$10$Gpw000A7Q.5jNTFT1gCTiONz7w0eYP1KyE8OiUD0AkhLxIzyAKfnC', 'test@pm.test', '0000-00-00'),
(21, 'test3', '$2y$10$MnP.cM19rTIYZOWlAD5e3ulmdmvukufkfDCxEFS2r.62A5QfAzB.6', 'test@pm.test', '0000-00-00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
  ADD CONSTRAINT `fk_countries_continents` FOREIGN KEY (`continent_code`) REFERENCES `continents` (`code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
